module Main where

-- flags
import System.Environment

-- opengl
import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT

-- io
import Data.IORef
import System.IO.Unsafe

-- stuff
import Control.Monad

terminate_program = do
  -- INSERT tear down stuff
  leaveMainLoop

main = do 
  args <- getArgs
  -- INSERT initialize stuff
  -- window
  glut_init

glut_init = do
  (progname, _) <- getArgsAndInitialize
  initialDisplayMode $= [DoubleBuffered]
  createWindow "opengl window" 
  displayCallback $= display 
  reshapeCallback $= Just (reshape)
  keyboardMouseCallback $= Just (keymouse)
  motionCallback $= Just (mousemove)
  mainLoop

{- GLUT functions -}
display = do
  clear [ColorBuffer]
  loadIdentity
  swapBuffers

redraw = postRedisplay Nothing

reshape s@(Size w h) = do
  viewport $= (Position 0 0, s)
  postRedisplay Nothing

keymouse key state modifiers position =
  if state == Up then return () else
  case key of
    Char 'q' -> terminate_program
    other -> return ()

mousemove position = redraw
